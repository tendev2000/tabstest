import UIKit

class SecondViewController: UIViewController {

    var timer:Timer?
    var seconds = 15
    
    @IBOutlet weak var textLabel: UILabel!
    @IBAction func startTimerTapped(_ sender: Any) {
        
        if timer == nil {
            timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { timer in
                if self.seconds == 0 {
                    self.finishGame()
                } else if self.seconds <= 15 {
                    self.seconds -= 1
                    self.updateTimeLabel()
                }
            }
        }
    }
    
    func finishGame()
    {
        timer?.invalidate()
        timer = nil
        
        let alert = UIAlertController(title: "Time's Up!", message: "Your time is up! You got a score of 100 points. Awesome!", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK, start new game", style: .default, handler: nil))

        self.present(alert, animated: true, completion: nil)
        
        seconds = 15
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateTimeLabel()
    }

    func updateTimeLabel() {

        let min = (seconds / 60) % 60
        let sec = seconds % 60

        textLabel?.text = String(format: "%02d", min) + ":" + String(format: "%02d", sec)
    }

}

