import UIKit

class CustomTabBarController: UITabBarController {
    
    var isViewVisible = false
    var containerView = UIView()
    var viewHeight : NSLayoutConstraint?
    
  override func viewDidLoad() {
    super.viewDidLoad()

    //...do some of your custom setup work
    // add a container view above the tabBar
    // containerView = UIView()
    containerView.backgroundColor = .yellow
    containerView.layer.borderColor = UIColor.blue.cgColor
    containerView.layer.borderWidth = 2.0

    view.addSubview(containerView)
    containerView.translatesAutoresizingMaskIntoConstraints = false
    containerView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
    containerView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true

    // anchor your view right above the tabBar
    // containerView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
    containerView.bottomAnchor.constraint(equalTo: tabBar.topAnchor).isActive = true

    
    viewHeight = containerView.heightAnchor.constraint(equalToConstant: 5) // .isActive = true
    viewHeight?.isActive = true
    
    // toggleView()
  }
    
        func toggleView() {
            isViewVisible = !isViewVisible
            
    //        UIView.animate(withDuration: 1.0, animations: {
    //            self.view.frame.size.height += 100
    //            self.view.frame.size.width += 100
    //
    //
    //        })
            
            
            UIView.animate(withDuration: 0.5) {
                // self.containerView.frame.size.height = self.isViewVisible ? 300 : 0
                if (self.isViewVisible) {
                    // self.containerView.heightAnchor.constraint(equalToConstant: 300).isActive = true
                    self.viewHeight?.constant = 100
                }
                else {
                    self.viewHeight?.constant = 5
                    // self.containerView.heightAnchor.constraint(equalToConstant: 10).isActive = true
                }
                self.view.layoutIfNeeded()
            }
        }
}
