import UIKit

class FirstViewController: UIViewController {
    
    @IBOutlet weak var myImage: UIImageView!
    @IBAction func buttonTapped(_ sender: Any) {
        
        guard let validCell = myImage else { return }
        UIView.animate(withDuration: 0.25, delay: 0, options: .curveLinear, animations: {
            
            validCell.transform = validCell.transform.rotated(by: CGFloat(Double.pi))
            self.myImage.tintColor = UIColor.random
            
        }) { finished in
            // self.tapAction()
        }
        
    }
    @IBAction func toggleTopViewButtonTapped(_ sender: Any) {
        let tbc = tabBarController as? CustomTabBarController
        tbc?.toggleView()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        let tapGesture = UITapGestureRecognizer(target: view, action: #selector(UIView.endEditing))
        view.addGestureRecognizer(tapGesture)
        // If your tap gesture blocked some other touches, then add this line:
        // tapGesture.cancelsTouchesInView = false
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }

    func resignTextFieldFirstResponders() {
      for textField in self.view.subviews where textField is UITextField {
        textField.resignFirstResponder()
      }
    }

    func resignAllFirstResponders() {
        view.endEditing(true)
    }
    
    @IBAction func backAction(_ sender: Any) {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
}

extension UIColor {
    static var random: UIColor {
        return UIColor(red: .random(in: 0...1),
                       green: .random(in: 0...1),
                       blue: .random(in: 0...1),
                       alpha: 1.0)
    }
}

